@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3" >
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Pertanyaan {{$pertanyaan1->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan/{{$pertanyaan1->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" name="judul" id="judul" value="{{old('judul', $pertanyaan1->judul)}}" placeholder="Enter Judul" required>
                    @error('judul')
                         <div class="alert alert-denger">{{$message}}</div>
                    @enderror    
                </div>
                  <div class="form-group">
                    <label for="isi">Isi</label>
                    <input type="text" class="form-control" id="isi" name="isi" value="{{old('isi', $pertanyaan1->isi)}}" placeholder="isi" required>
                    @error('isi')
                         <div class="alert alert-denger">{{$message}}</div>
                    @enderror  
                </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>
</div>
@endsection